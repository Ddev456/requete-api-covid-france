<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchDepartmentController extends AbstractController
{
    /**
     * @Route("/search/department", name="search_department")
     */
    public function index(): Response
    {
        return $this->render('search_department/index.html.twig', [
            'controller_name' => 'SearchDepartmentController',
        ]);
    }
}
