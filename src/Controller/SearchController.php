<?php

namespace App\Controller;

use DateTime;
use App\Service\ApiService;
use Symfony\UX\Chartjs\Model\Chart;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function index(ApiService $callApiService, ChartBuilderInterface $chartBuilder, $dataSearch = null): Response
    {
        $label = [];
        $hospitalisation = [];
        $rea = [];

        // $tableau = ['Ain','Aisne'];
        
        // for($i=0; $i<count($tableau); $i++){
        // if($dataSearch != $tableau[$i]){
        //     $dataSearch = null;
        //     $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        // }else{

        

  
        for ($i=1; $i < 8; $i++) { 
            $date = New DateTime('- '. $i .' day');
            $datas = $callApiService->getAllDataByDate($date->format('Y-m-d'));

            foreach ($datas['allFranceDataByDate'] as $data) {
                if( $data['nom'] === $dataSearch) {
                    $label[] = $data['date'];
                    $hospitalisation[] = $data['nouvellesHospitalisations'];
                    $rea[] = $data['nouvellesReanimations'];
                    break;
                }
            }
        }

        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $chart->setData([
            'labels' => array_reverse($label),
            'datasets' => [
                [
                    'label' => 'Nouvelles Hospitalisations',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_reverse($hospitalisation),
                ],
                [
                    'label' => 'Nouvelles entrées en Réa',
                    'borderColor' => 'rgb(46, 41, 78)',
                    'data' => array_reverse($rea),
                ],
            ],
        ]);

        $chart->setOptions([/* ... */]);

       
        return $this->render('search_department/index.html.twig', [
            'data' => $callApiService->getDepartmentData($dataSearch),
            'chart' => $chart,
        ]);
        }
    }
// }